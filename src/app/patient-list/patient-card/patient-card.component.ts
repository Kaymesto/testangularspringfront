import { Component, Input, OnInit } from '@angular/core';
import { Patient } from './../../shared/models/Patient';
import { PatientService } from './../../shared/services/patient.service';

@Component({
  selector: 'app-patient-card',
  templateUrl: './patient-card.component.html',
  styleUrls: ['./patient-card.component.scss']
})
export class PatientCardComponent implements OnInit {

  @Input() patient!: Patient;

  constructor(private patientService: PatientService) { }

  ngOnInit(): void {
  }

  delete(patient: Patient): void {
    this.patientService.deletePatient(patient).subscribe((resp) => {
      console.log(resp);
      this.patientService.refreshPatients();
    });
  }

}
