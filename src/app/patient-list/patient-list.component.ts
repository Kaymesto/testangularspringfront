import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Patient } from '../shared/models/Patient';
import { PatientService } from './../shared/services/patient.service';

@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.scss']
})
export class PatientListComponent implements OnInit {

  patients: Patient[] = [];
  patientsSub?: Subscription;

  constructor(private patientService: PatientService) { }

  ngOnInit(): void {
    this.patientService.patientsObs.subscribe((patients: Patient[]) => {
      this.patients = patients;
    })
    this.getPatients();
  }

  // destructeur du composant
  ngOnDestroy() {
    if (this.patientsSub) {
      // désinscription à l'opbservable (libération des ressources)
      this.patientsSub.unsubscribe();
    }
  }

  getPatients() {
    this.patientService.refreshPatients();
  }

}
