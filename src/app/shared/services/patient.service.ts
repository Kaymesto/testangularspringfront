import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { Patient } from '../models/Patient';
import { environment } from './../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PatientService {

  private patientsSubject: BehaviorSubject<Patient[]> = new BehaviorSubject<Patient[]>(<Patient[]>[]);

  patientsObs: Observable<Patient[]> = this.patientsSubject.asObservable();


  constructor(private http: HttpClient) { }


  /**
 * Récupérer les patients
 * @param
 * @returns Observable<Patient[]>
 */
  getPatients(): Observable<Patient[]> {
    return this.http.get<Patient[]>(`${environment.typicode_url}/patients/`);
  }
  /**
   * récupérer le patient
   * @param id du patient à récupérer
   * @returns Observable<Patient>
   */
  getPatient(id: number): Observable<Patient> {
    return this.http.get<Patient>(`${environment.typicode_url}/patients/${id}`);
  }

  refreshPatients(): void {
    this.http.get<Patient[]>(`${environment.typicode_url}/patients/`)
      .subscribe((patients: Patient[]) => {
        // Changement de la valeur portée par le subject
        this.patientsSubject.next(patients);
        // Tous ce qui subscribe à userObs sera notifié et recevra la valeur du behavior subject
      })
  }

  /**
 * Crée le patient
 * @param patient à créer
 * @returns Observable<Patient>
 */
  postPatient(patient: Patient): Observable<Patient> {
    return this.http.post<Patient>(`${environment.typicode_url}/patients/create`, patient);;
  }

  /**
   * Update le patient
   * @param patient à update
   * @returns Observable<Patient>
   */
  putPatient(patient: Patient): Observable<Patient> {
    return this.http.post<Patient>(`${environment.typicode_url}/patients/update`, patient);;
  }


  /**
 * Supprime un Patient
 * @param patient le Patient à supprimer
 * @returns Observable<any>
 */
  deletePatient(patient: Patient): Observable<any> {
    return this.http.delete(`${environment.typicode_url}/patients/delete/${patient.id}`);
  }

}
