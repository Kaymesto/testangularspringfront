import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Infirmiere } from './../models/Infirmiere';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from './../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InfirmiereService {

  private infirmieresSubject: BehaviorSubject<Infirmiere[]> = new BehaviorSubject<Infirmiere[]>(<Infirmiere[]>[]);

  infirmieresObs: Observable<Infirmiere[]> = this.infirmieresSubject.asObservable();


  constructor(private http: HttpClient) { }


  /**
 * Récupérer les infirmieres
 * @param
 * @returns Observable<Infirmiere[]>
 */
  getInfirmieres(): Observable<Infirmiere[]> {
    return this.http.get<Infirmiere[]>(`${environment.typicode_url}/infirmieres/`);
  }
  /**
   * récupérer l'infirmiere
   * @param id de l'infirmiere à récupérer
   * @returns Observable<Patient>
   */
  getInfirmiere(id: number): Observable<Infirmiere> {
    return this.http.get<Infirmiere>(`${environment.typicode_url}/infirmieres/${id}`);
  }

  refreshInfirmieres(): void {
    this.http.get<Infirmiere[]>(`${environment.typicode_url}/infirmieres/`)
      .subscribe((infirmieres: Infirmiere[]) => {
        // Changement de la valeur portée par le subject
        this.infirmieresSubject.next(infirmieres);
        // Tous ce qui subscribe à userObs sera notifié et recevra la valeur du behavior subject
      })
  }

  /**
 * Crée l'infirmiere
 * @param infirmiere à créer
 * @returns Observable<Patient>
 */
  postInfirmiere(infirmiere: Infirmiere): Observable<Infirmiere> {
    return this.http.post<Infirmiere>(`${environment.typicode_url}/infirmieres/create`, infirmiere);;
  }

  /**
   * Update l'infirmieres
   * @param infirmieres à update
   * @returns Observable<Infirmiere>
   */
  putInfirmiere(infirmiere: Infirmiere): Observable<Infirmiere> {
    return this.http.post<Infirmiere>(`${environment.typicode_url}/infirmieres/update`, infirmiere);;
  }


  /**
 * Supprime une infirmiere
 * @param infirmieres l'infirmieres à supprimer
 * @returns Observable<any>
 */
  deleteInfirmiere(infirmiere: Infirmiere): Observable<any> {
    return this.http.delete(`${environment.typicode_url}/infirmieres/delete/${infirmiere.id}`);
  }

}
