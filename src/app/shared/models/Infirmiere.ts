export interface Infirmiere {
    id: number,
    nom: String,
    prenom: String,
    tel_pro: String,
    tel_perso: String,
    numero_professionnel: number,
    adresseinfirmiere: Adresse,
    deplacements: Deplacement[],

}

interface Adresse {
    rue: String,
    numero: String,
    cp: String,
    ville: String
}

interface Deplacement {
    id: number,
    cout: number,
    date: Date,
    patient: Patient,
    infirmiere: Infirmiere
}

interface Patient {
    id: number,
    nom: String,
    prenom: String,
    date_de_naissance: Date,
    sexe: String,
    numero_securite_sociale: Number,
    adressepatient: Adresse,
}
