export interface Patient {
    id: number,
    nom: String,
    prenom: String,
    date_de_naissance: Date,
    sexe: String,
    numero_securite_sociale: Number,
    adressepatient: Adressepatient,
    infirmiere: Infirmiere
}

interface Adressepatient {
    rue: String,
    numero: String,
    cp: String,
    ville: String
}

interface Infirmiere {
    id: number;
    nom: "",
    prenom: "",
    tel_pro: "",
    tel_perso: "",
    numero_professionnel: ""
}