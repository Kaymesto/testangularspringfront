import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import { Patient } from '../shared/models/Patient';
import { PatientService } from './../shared/services/patient.service';
import { ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-add-patient',
  templateUrl: './add-patient.component.html',
  styleUrls: ['./add-patient.component.scss']
})
export class AddPatientComponent implements OnInit {
  patientForm: FormGroup;

  constructor(private fb: FormBuilder, private patientService: PatientService) {
    this.patientForm = this.fb.group({
      nom: new FormControl("", [Validators.required, Validators.minLength(3), Validators.maxLength(50)]),
      prenom: new FormControl("", Validators.required),
      date_de_naissance: new FormControl("", Validators.required),
      sexe: new FormControl("", Validators.required),
      numero_securite_sociale: new FormControl("", Validators.required),
      adressepatient: new FormGroup({
        numero: new FormControl(),
        rue: new FormControl(),
        ville: new FormControl(),
        cp: new FormControl(),
      }),
      infirmiere: new FormGroup({
        id: new FormControl("", Validators.required),
      })
    });
  }

  ngOnInit(): void {
  }

  addPatient(): void {
    if (this.patientForm.status === "VALID") {
      console.log("mon form", this.patientForm.value);
      this.patientService.postPatient(this.patientForm.value).subscribe((newPatient: Patient) => {
        console.log("MON NEW PATIENT", newPatient);
        this.patientForm.reset();
      });
    }
    console.log("patient form", this.patientForm.value);
  }

}
