import { Component, OnInit } from '@angular/core';
import { Infirmiere } from '../shared/models/Infirmiere';
import { Subscription } from 'rxjs';
import { InfirmiereService } from '../shared/services/infirmiere.service';

@Component({
  selector: 'app-infirmiere-list',
  templateUrl: './infirmiere-list.component.html',
  styleUrls: ['./infirmiere-list.component.scss']
})
export class InfirmiereListComponent implements OnInit {

  infirmieres: Infirmiere[] = [];
  infirmieresSub?: Subscription;

  constructor(private infirmiereService: InfirmiereService) { }

  ngOnInit(): void {
    this.infirmiereService.infirmieresObs.subscribe((infirmieres: Infirmiere[]) => {
      this.infirmieres = infirmieres;
    })
    this.getInfirmieres();
  }

  // destructeur du composant
  ngOnDestroy() {
    if (this.infirmieresSub) {
      // désinscription à l'opbservable (libération des ressources)
      this.infirmieresSub.unsubscribe();
    }
  }

  getInfirmieres() {
    this.infirmiereService.refreshInfirmieres();
  }
}
