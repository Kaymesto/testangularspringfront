import { Component, OnInit, Input } from '@angular/core';
import { Infirmiere } from './../../shared/models/Infirmiere';
import { InfirmiereService } from './../../shared/services/infirmiere.service';

@Component({
  selector: 'app-infirmiere-card',
  templateUrl: './infirmiere-card.component.html',
  styleUrls: ['./infirmiere-card.component.scss']
})
export class InfirmiereCardComponent implements OnInit {

  @Input() infirmiere!: Infirmiere;


  constructor(private infirmiereService: InfirmiereService) { }

  ngOnInit(): void {
  }

  delete(infirmiere: Infirmiere): void {
    this.infirmiereService.deleteInfirmiere(infirmiere).subscribe((resp) => {
      console.log(resp);
      this.infirmiereService.refreshInfirmieres();
    });
  }

  displayDeplacements(infirmiere: Infirmiere): void {

  }


}
