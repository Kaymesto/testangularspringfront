import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PatientListComponent } from './patient-list/patient-list.component';
import { DeplacementListComponent } from './deplacement-list/deplacement-list.component';
import { AdresseListComponent } from './adresse-list/adresse-list.component';
import { InfirmiereListComponent } from './infirmier-list/infirmiere-list.component';
import { PatientCardComponent } from './patient-list/patient-card/patient-card.component';
import { DeplacementCardComponent } from './deplacement-list/deplacement-card/deplacement-card.component';
import { AdresseCardComponent } from './adresse-list/adresse-card/adresse-card.component';
import { InfirmiereCardComponent } from './infirmier-list/infirmiere-card/infirmiere-card.component';
import { AddInfirmierComponent } from './add-infirmier/add-infirmier.component';
import { AddPatientComponent } from './add-patient/add-patient.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NavbarComponent } from './shared/component/navbar/navbar.component';

@NgModule({
  declarations: [
    AppComponent,
    PatientListComponent,
    DeplacementListComponent,
    AdresseListComponent,
    InfirmiereListComponent,
    PatientCardComponent,
    DeplacementCardComponent,
    AdresseCardComponent,
    InfirmiereCardComponent,
    AddInfirmierComponent,
    AddPatientComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
