import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddInfirmierComponent } from './add-infirmier/add-infirmier.component';
import { AddPatientComponent } from './add-patient/add-patient.component';
import { InfirmiereListComponent } from './infirmier-list/infirmiere-list.component';
import { PatientListComponent } from './patient-list/patient-list.component';

const routes: Routes = [

  { path: '', redirectTo: 'patients', pathMatch: 'full' },
  { path: 'home', component: PatientListComponent },
  {
    path: 'patients',
    children: [
      { path: '', pathMatch: 'full', component: PatientListComponent },
      { path: 'create', pathMatch: 'full', component: AddPatientComponent },
    ],
  },
  {
    path: 'infirmieres',
    children: [
      { path: '', component: InfirmiereListComponent, pathMatch: 'full' },
      { path: 'create', component: AddInfirmierComponent, pathMatch: 'full' },
      // { path: 'update/:id', component: UpdateInfirmierComponent, pathMatch: 'full' },
      // { path: ':id', component: DetailInfirmierComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
