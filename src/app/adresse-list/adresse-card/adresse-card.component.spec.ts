import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdresseCardComponent } from './adresse-card.component';

describe('AdresseCardComponent', () => {
  let component: AdresseCardComponent;
  let fixture: ComponentFixture<AdresseCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdresseCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdresseCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
