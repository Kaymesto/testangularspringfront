import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeplacementCardComponent } from './deplacement-card.component';

describe('DeplacementCardComponent', () => {
  let component: DeplacementCardComponent;
  let fixture: ComponentFixture<DeplacementCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeplacementCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeplacementCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
