import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { InfirmiereService } from './../shared/services/infirmiere.service';
import { Infirmiere } from './../shared/models/Infirmiere';

@Component({
  selector: 'app-add-infirmier',
  templateUrl: './add-infirmier.component.html',
  styleUrls: ['./add-infirmier.component.scss']
})
export class AddInfirmierComponent implements OnInit {
  infirmiereForm: FormGroup;

  constructor(private fb: FormBuilder, private infirmiereService: InfirmiereService) {
    this.infirmiereForm = this.fb.group({
      nom: new FormControl("", [Validators.required, Validators.minLength(3), Validators.maxLength(50)]),
      prenom: new FormControl("", Validators.required),
      tel_pro: new FormControl("", Validators.required),
      tel_perso: new FormControl("", Validators.required),
      numero_professionnel: new FormControl("", Validators.required),
      adresseinfirmiere: new FormGroup({
        numero: new FormControl(),
        rue: new FormControl(),
        ville: new FormControl(),
        cp: new FormControl(),
      }),

    });
  }

  ngOnInit(): void {
  }

  addInfirmiere(): void {
    if (this.infirmiereForm.status === "VALID") {
      console.log("mon form", this.infirmiereForm.value);
      this.infirmiereService.postInfirmiere(this.infirmiereForm.value).subscribe((newInfirmiere: Infirmiere) => {
        console.log("MON NEW Infirmiere", newInfirmiere);
        // this.infirmiereForm.reset();
      });
    }
    console.log("Infirmiere form", this.infirmiereForm.value);
  }
}
